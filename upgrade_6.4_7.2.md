# aggiornare proxmox cluster [5 nodi] da 6.4 a 7.2
# verificare compatibilità proxmox backup server 2.04?

## passaggi di controllo prima dell aggiornamento di versione da effettuare su ogni nodo
- avere PVE alla 6.4
- avere ceph alla 15.2 octopus  [guida aggiornamento ceph](https://pve.proxmox.com/wiki/Ceph_Nautilus_to_Octopus) **da aggiornare alla successiva versione 16.2 pacific causa EOL**
- avere Proxmox Backup Server alla 2.x
- per sicurezza avere accesso fisico alla macchina oltre ad una connesione ssh
- almeno 4GB di spazio libero nella /
- il cluster ready in ogni nodo
- assicurarsi che l'utente ROOT abbia una password inserita
- eseguire lo script di verifica su ogni nodo per verificare i possibili warnings

`pve6to7 --full`

## procedura da effettuare in ogni nodo del cluster per avanzamento di versione
- eventuale creazione di un nodo temporaneo 7.2 di supporto per migrazione VM/LXc
- spostare tuttle le VM e LXC presenti in altri nodi **da vecchia verisone PVE a nuova versione PVE -> OK**
- nel avanzamento potrebbe cambiare il MAC del linux bridge provocando possibili sovrapposizioni, soluzioni:
  1. avere il pkg **ifupdown2** già alla versione 3.1.0-1 prima dell avanzamento
  2. forzare il MAC vecchio definendolo nel file `/etc/network/interfaces`

     eseguire il commando per ottenere il MAC: `ip -c link`

     modificare il file:
    ```
        auto vmbr0
          iface vmbr0 inet static
          address 192.168.X.Y/24
          hwaddress aa:bb:cc:12:34 <<-- inserire MAC vecchio da mantenere
    ```
  - forzare aggiornamento dello stack di rete `ifreload -a` oppure `ifdown vmbr0; ifup vmbr0`
- assicurarsi di avere le ultimissime versioni dei pkgs 6.4 `apt update && apt dist-upgrade`
- aggiornare i repo debian da buster a bullseye 

`sed -i 's/buster\/updates/bullseye-security/g;s/buster/bullseye/g' /etc/apt/sources.list`
- disabilitare tutti i repo relativi alla versione 6.4 di proxmox
- aggiungere i repo della versione 7.2 enterprise 

`echo "deb https://enterprise.proxmox.com/debian/pve bullseye pve-enterprise" > /etc/apt/sources.list.d/pve-enterprise.list`
  - per versione repo no-subscription:
    ```
        deb http://ftp.debian.org/debian bullseye main contrib
        deb http://ftp.debian.org/debian bullseye-updates main contrib

        # PVE pve-no-subscription repository provided by proxmox.com,
        # NOT recommended for production use
        deb http://download.proxmox.com/debian/pve bullseye pve-no-subscription

        # security updates
        deb http://security.debian.org/debian-security bullseye-security main contrib
    ```
- sostituire i repo ceph.com dai proxmox.com ceph 

`echo "deb http://download.proxmox.com/debian/ceph-octopus bullseye main" > /etc/apt/sources.list.d/ceph.list`
- rimuovere tutti i repo **backports**
- verifica correttezza dei repo abilitati
- aggionare i repository 

`apt update`
- eseguire avanzamento di versione 

`apt dist-upgrade`
- se l'avanzamento viene eseguito correttamente fare un reboot per caricare il nuovo kernel

## verifica finale su ogni nodo
- controllare lo stato dei pkg che siano tutti all'ultima versione disponibile
- aggiornare ceph dalla 15.2 alla 16.2 causa EOL [guida aggiornamento ceph](https://pve.proxmox.com/wiki/Ceph_Octopus_to_Pacific)

## risorse utili per risoluzione problemi:
- [debian](https://www.debian.org/releases/bullseye/amd64/release-notes/ch-information.en.html)
- [proxmox](https://pve.proxmox.com/wiki/Roadmap#7.0-known-issues)
- [proxmox live migration](https://forum.proxmox.com/threads/live-migration-zero-downtime.34881/#post-170940)
- [possible problema di compatibilità con grub della nuova versione di ZFS](https://pve.proxmox.com/wiki/ZFS:_Switch_Legacy-Boot_to_Proxmox_Boot_Tool)