# snippet e programmi per gestire e controllare l'uso di ram/swap

- usare questo script per avere una lista dei processi che usano molta swap

 `for file in /proc/*/status ; do awk '/VmSwap|Name/{printf $2 " " $3}END{ print ""}' $file; done | sort -k 2 -n -r | head -n 15`

 - usare il programma **smem**

 `yum install smem`
 `smem -p -s swap`

- per resettare l'uso della swap in caso ci sia abbastanza spazion in ram

`swapoff -a`
`swapon -a`