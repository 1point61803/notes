# come abilitare la dashboard interna di ceph

- installare il pkg con i tools
`apt install ceph-mgr-dashboard`

- autogenerare un nuovo certificato SSL
`ceph dashboard create-self-signed-cert`

- creare un account aministrativo per accedere alla dashboard
`ceph dashboard ac-user-create <username> -i <file-containing-password> administrator`

- disabilitare la sospensione del account a causa di eccessivo numero di errori
`ceph dashboard set-account-lockout-attempts 0`

- impostare gli IP e porta per la dashboard
`ceph config set mgr mgr/dashboard/<mgr ID>/server_addr $IP`
`ceph config set mgr mgr/dashboard/<mgr ID>/server_port $PORT`
`ceph config set mgr mgr/dashboard/<mgr ID>/ssl_server_port $PORT`

???? TODO  da finire