# avanzamento ceph da 15 a 16

## passagi di controllo prima dell avanzamento di versione
- avere ceph alla versione 15.x
- aver proxmox alla versione 7.x
- nell aggiornamento al primo lancio del demone che gestisce OSD può prendere diverso tempo nella fase di formattazione in base alla mole di dati da trattare
- individuare in quali nodi risulta presente il **ceph monitor**

## procedura da effettuare su ogni nodo
- abilitare in ogni nodo la seconda verisione del protocollo

`ceph mon enable-msgr2`
controllare che le porte abilitate siano 6789 ->v1 3300 ->v2
- nel caso di passaggio alla 16.2.6 si neccessita di bloccare la migrazione automatica dei dati per evitare il loro degrado [**causa bug**](https://tracker.ceph.com/issues/53062)

`ceph config set osd bluestore_fsck_quick_fix_on_mount false`

se si fa l'upgrade alla versione 16.2.7 non c'è la neccessità di posticipare la migrazione in quanto il bug é stato risolto
- cambiare i repo verso i pacific

`sed -i 's/octopus/pacific/' /etc/apt/sources.list.d/ceph.list`
- estromettere il nodo ceph dal cluster 

`ceph osd set noout`
- effettuare l'avanzamento di versione

`apt update && apt full-upgrade`
- dopo aver effettuato l'aggiornamento su tutti i nodi riavviare il servizio di monitoraggio dove presente **ceph monitor**

`systemctl restart ceph-mon.target`
- verifcare la nuova versione di ceph

`ceph mon dump | grep min_mon_release` <<--16
- successivamente riavviare in tutti i nodi il servizio **ceph manager**

`systemctl restart ceph-mgr.target`
- verificare il coretto funzionamaneto eseguendo

`ceph -s` <<-- mgr: foo(active)
- riavviare **in un nodo alla volta** il servizio **OSD**, il riavvio richiederà del tempo per effettura la migrazione

`systemctl restart ceph-osd.target`

c'è la possibiltà di disabilitare questa migrazione eseguendo il commando sotto riportato prima di eseguire il restart del servizio OSD, ma va comunque fatta:
`ceph config set osd bluestore_fsck_quick_fix_on_mount false`

- verificare il coretto funzionamaneto eseguendo

`ceph -s` <<-- HEALTH_OK oppure HEALTH_WARN noout flag(s) set

- abiliatre il cluster ceph solo nella versione 16

`ceph osd require-osd-release pacific`
- prendere nota di ogni servizio **MDS metadata server** relativo ad ogni **ceph FS**e dei rispettivi rank impostati
- ridurre il numero di rank di ogni MDS a 1

`ceph fs set $FS_NAME max_mds 1`

aspettare che il cambio venga attuato dal cluset ceph
    
    ceph status
    ceph fs get <fs_name> | grep max_mds
    ceph fs set <fs_name> max_mds 1

- fermare i servizi mds sopsesi sui rispettivi host

`systemctl stop ceph-mds.target`

verificare la sola presenza di 1 MDS sul rank 0
- riavviare forzando l'aggiornamento del unico servizio MDS 

`systemctl restart ceph-mds.target`
- riavviare tutti i servizi MDS terminati precedentemente sui i vari nodi

`systemctl start ceph-mds.target`
- ripristinare il numero di ranks per il MDS

`ceph fs set $FS_NAME max_mds $RANKS_MAX`
- reinserire il nodo nel ceph cluster

`ceph osd unset noout`
- verifcare la possiblità di attivare autoscaling sui pool appena aggiornati [1](https://forum.proxmox.com/threads/ceph-octopus-upgrade-notes-think-twice-before-enabling-auto-scale.80105/)
 
`ceph osd pool set $POOLNAME pg_autoscale_mode on`