# creazione template alma8.6 con plesk e altre adattamenti (imunify360 ???)

- installazione alma 8.6
  - partizionamento di un disco piccolo 15GB:
    - boot (ext4)
    - swap
    - lvm2
      - root (xfs: defaults,usrquota 1 1)
    ---- alternativa
      - root (xfs: defaults,usrquota 1 1)
      - tmp (ext4: defaults,noexec,nosuid,nouser 1 2)

  - settare una password di root robusta

- impostare data/ora e timezone

- abilitazione interfaccia di rete e cambio hostname con il tools **nmtui**

- abilitazione dns interni via **/etc/resov.conf**
  - 94.143.153.38
  - 94.143.154.38

- aggiornamento del sistema

`yum update`

- installare i pkg
  - vim
  - wget
  - postfix

- impostare gli alias per bash
  - aggiungere la stringa per avree il timestamp dei comandi su **/etc/profile**

  `HISTTIMEFORMAT="%Y%m%d - %H:%M:%S - "`
  - aggiungere degli alias per il profilo bash di root **/root/.bashrc**
  ```
  alias a='alias'
  alias hi='history'
  alias grep='grep --color'
  alias ll='ls -al'
  ```

- installare plesk nella versione minima [hosting e poschi altri servizi]
  - eseguire installazione da CLI
  - eseguire primo login con dichiarazione prima admin password
  - disinstallare i componenti non utili (dns, mail,ssh terminal, etc..)
  - installare l'estensione plesk migrator
  - cambiare lingua in italiano
  - impostare account con permessi limitati

- testare invio mail via CLI

`echo "Questa è una prova" | mail -s "TEST" ivano.borsato@mvmnet.com`