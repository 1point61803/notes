# banale comando per inviare una mail di prova

- comando su una riga per inviare mail di prova

`echo "Questa è una prova" | mail -s "TEST" ivano.borsato@mvmnet.com`

## commandi per compilare una mail di prova ed inviarla

- collegarsi via telnet al server smtp

`telnet <SMTP SERVER> 25`

- identificarsi col proprio hostname

`helo <HOSTNAME completo>`

- specificare la mail di partenza

`mail from: <MAIL FROM>`

- specificare la mail di destinazione

`rcpt to: <MAIL TO>`

- dichiarare l'inizio della mail

`data`

- scrivere il corpo dell mail andare a capo inserire un carrattere . (punto) e andare a capo ancora

```
TEST,TEST,TEST 
.

```

- a questo punto la mail è stata inviata, per uscire eseguire il commando quit

`quit`