# importare da vmware [OVF] nel cluster proxmox

- avere l'immmagine ovf[.ovf + .vmdk + .mf] della VM
- verificare l'autenticità dell immaggine dall originale
- convertire l'immagine nel formato compatibile con QEMU
- importare nella VM l'immagine ovf
`qm importovf <VMID> <file>.ovf <storage>`
- creare una nuova VM in proxmox senza alcun SO [no CD o altro]
- eventualmente dalla webui di proxmox selezione nella VM il nuovo disco appena attaccato e cambiare il bus device in **VirtIOBlock**