# come implementare il monitoraggio di ceph su grafana via prometheus

- craere un LXC con un server prometheus
- abilitare sul firewall la porta 9090 per prometheus 
`firewall-cmd --add-port=9090/tcp --permanent`
- abilitare le porte 9283 e 9100 per il crapping del cluster ceph
`firewall-cmd --add-port=9283/tcp --add-port=9100/tcp --permanent`
- modificare il file di configurazione di prometheus indicando come IP dello scrapping un nodo ceph manager attivo
`CUT
scrape_configs:
  # The job name is added as a label `job=<job_name>` to any timeseries scraped from this config.
  - job_name: "prometheus"

    # metrics_path defaults to '/metrics'
    # scheme defaults to 'http'.

    static_configs:
      - targets: ["localhost:9090"]

  - job_name: 'ceph'
    static_configs:
      - targets: ['172.17.0.141:9283']

  - job_name: 'node-exporter'
    static_configs:
      - targets: ['172.17.0.141:9100']`

- abilitare il modulo su un nodo ceph dell esportazione verso prometheus
`ceph mgr module enable prometheus`

- verificare l'esecuzione del servizio con IP e porta corretto
`ceph mgr services`

- OPZIONALE cambiare IP e porta di uscita delle metriche
`ceph conf/ig set mgr mgr/prometheus/server_addr <IP>`
`ceph config set mgr mgr/prometheus/server_port <port>`

- verificare l'esportazione delle metriche
`curl http://172.17.0.141:9283/metrics`

