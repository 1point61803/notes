# procedura per intercettare su plesk le mail generate da file php

- creare uno script per sdoppiare il traffico diretto verso postfix

`vim /usr/sbin/sendmail.postfix-wrapper`
```
CONFIG_TEXT: #!/bin/sh
(echo X-Additional-Header: $PWD ;cat) | tee -a /var/tmp/mail.send|/usr/sbin/sendmail.postfix-bin "$@"
```

`chmod a+x /usr/sbin/sendmail.postfix-wrapper`

- crea il file destinazione per i log

`touch /var/tmp/mail.send`
`chmod a+rw /var/tmp/mail.send`

- sostituisci con lo script il binario di sendmail e crea un backup per quest'ultimo

`mv /usr/sbin/sendmail.postfix /usr/sbin/sendmail.postfix-bin`
`ln -s /usr/sbin/sendmail.postfix-wrapper /usr/sbin/sendmail.postfix`

- aspetta che il file si riempia e poi ritorna alla situazione iniziale sostituendo i binari

`mv /usr/sbin/sendmail.postfix /root/backup__sendmail.postfix`
`mv /usr/sbin/sendmail.postfix-bin /usr/sbin/sendmail.postfix`

- esamina il file di log

`grep X-Additional /var/tmp/mail.send | grep `cat /etc/psa/psa.conf | grep HTTPD_VHOSTS_D | sed -e 's/HTTPD_VHOSTS_D//' ``



