# simple tips and command to manage apache2

- You can easily check your Apache server’s version with the following command.i

`apache2 -v`

- If you want to detailed information about your Apache server, use -V option.

`apache2 -V`

- After you make changes to your Apache server’s configuration file, you can run the following command to test Apache configuration.

`apachectl -t`

- It will point out syntax errors in your configuration file so that you don’t restart Apache server with an erroneous configuration file.

- If you want to enable a virtual host for website, say, example.com, you need to place its configuration file at /etc/apache2/sites-available and use a2ensite command to create a symlink to the configuration file, at /etc/apache2/sites-enabled. For example, if you have /etc/apache2/sites-available/example.com.conf file then run the following command to enable it.

`a2ensite example.com`

- Similarly, if you want to disable the above virtual host, use a2dissite command.

`a2dissite example.com`


- Similarly, you can also enable or disable configuration files with a2enconf and a2disconf commands respectively. These configuration files are generally placed at /etc/apache2/sites-available.

- Here is an example command to enable phpmyadmin configuration file.

`a2enconf phpmyadmin`

- Similarly, here is the command to disable this configuration file.

`a2disconf phpmyadmin`

- Apache supports wide range of modules, packages & libraries to extend its functionality. The modules are installed at /etc/apache2/mods-available. When you enable a module, a symlink to the module will be created at /etc/apache2/mods-enabled.

- You can use a2enmod command to enable Apache module, and a2dismod to disable the module. Here is an example to enable Apache’s mod_rewrite module.

`a2enmod rewrite`

- Here is the command to disable mod_rewrite.

`a2dismod rewrite`

- You can use systemctl utility to restart Apache server easily. Here are the common commands to manage Apache server.
    - Start Server

    `systemctl start apache2`

    - Stop Server

    `systemctl stop apache2`

    - Restart Server

    `systemctl restart apache2`

    - Reload Apache Configuration Without Restart

    `systemctl reload apache2`
