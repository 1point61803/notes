# creare un dump del DB

- login come admin
`MYSQL_PWD=`cat /etc/psa/.psa.shadow` mysql -u admin`

 - query fatta in 0.019sec
 29 rows in set (0,019 sec) 

- seleziono il DB
 `use <DB NAME>;`

- eseguo la query per verificar ela correttezza del DB
`select <QUERY>;`

- eseguire il dump del DB
`MYSQL_PWD=`cat /etc/psa/.psa.shadow` mysqldump -u admin acq_db_67wsvjy > /root/acq_db_67wsvjy_BK.sql`