# appunti per la creazioni di nuove VM

- partizione di / e swap, in casi particolari creare una partizione di /tmp col flag noexec e altri flag per aumentare la sicurezza del server

- password root robusta

- installare e abilitare il servizio qemu-guest-agent

- molta attenzione a configuarre la rete con IP liberi

- impostare la giusta scheda di rete con il corretto tag vlan

ip 10.0.2.15 /24
gw 10.0.2.2