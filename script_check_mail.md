# script in perl che analizza 


#!/usr/bin/perl -w

## https://kb.plesk.com/114845
## create as /usr/sbin/sendmail.postfix-wrapper
## chmod a+x /usr/sbin/sendmail.postfix-wrapper
## mv /usr/sbin/sendmail.postfix /usr/sbin/sendmail.postfix-bin
## ln -s /usr/sbin/sendmail.postfix-wrapper /usr/sbin/sendmail.postfix

## create /var/log/formmail 0666
## enable logrotation

## test via website, doesn't work via webmail (is smtp)

# use strict;
use Env;
my $date = `date`;
chomp $date;
open (INFO, ">>/var/log/formmail") || die "Failed to open file ::$!";
my $uid = $>;
my @info = getpwuid($uid);
#if($REMOTE_ADDR) {
#print INFO "$date - $REMOTE_ADDR ran $SCRIPT_NAME at $SERVER_NAME \n";
#}
#else {
print INFO "$date - $PWD - @info\n";
#}
my $mailprog = '/usr/sbin/sendmail.postfix-bin';
foreach (@ARGV) {
$arg="$arg" . " $_";
}
open (MAIL,"|$mailprog $arg") || die "cannot open $mailprog: $!\n";
while (<STDIN> ) {
print MAIL;
}
close (INFO);
close (MAIL);