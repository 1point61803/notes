# installare e configurare un agente zabbix su lnux server

- installazione via repo

https://www.zabbix.com/documentation/3.2/en/manual/installation/install_from_packages/agent_installation

- installazione del pkg relativo all agent zabbix (valutare nuove versioni)

`rpm -ivh http://repo.zabbix.com/zabbix/3.2/rhel/7/x86_64/zabbix-release-3.2-1.el7.noarch.rpm`
`yum install zabbix-agent`

- configurazione del file **/etc/zabbix/zabbix_agentd.conf**
```
EnableRemoteCommands=1
Server=94.143.152.30
#ServerActive=127.0.0.1
Hostname=<HOSTNAME>
```

- creare e modifica proprietario e permessi alla cartella **/var/lib/zabbix/**

`mkdir /var/lib/zabbix`
`chown -R zabbix:zabbix /var/lib/zabbix`
`chmod 500 /var/lib/zabbix/`

- crea w modifica il proprietario più i permessi per il file **.my.cnf**

`touch /var/lib/zabbix/.my.cnf`
`chmod 400 /var/lib/zabbix/.my.cnf`
`chown -R zabbix:zabbix /var/lib/zabbix`

- modificare il file sopra con questi paramentri ???

```
[client]
user = zabbix
password = <password>
```

- accedere come utente admin al DB con credenziali di plesk

`mysql -uadmin -p`cat /etc/psa/.psa.shadow``

- creare un utente per zabbix nel DB mysql

`USE mysql;`
`CREATE USER 'zabbix'@'%' IDENTIFIED BY 'ZBX_monit_agent_PwD18';`
`GRANT REPLICATION CLIENT,PROCESS,SHOW DATABASES,SHOW VIEW ON *.* TO 'zabbix'@'%';`
`FLUSH PRIVILEGES;`

- abilitare nella macchina target una regola firewall che permette la comunicazione dal IP 94.143.152.30 sulla porta 10050

`firewall-cmd --permanent --zone=plesk --add-rich-rule='rule family="ipv4" source address="94.143.152.30/32" port protocol="tcp" port="10050" accept'`
`firewall-cmd --reload`

- controlla lo stato di attivazione della regola

`firewall-cmd --permanent --list-all-zones | grep -A 30  plesk | grep "rule family"`

- abilita il servizio zabbix all'avvio della macchina e forza l'avvio al momento

`systemctl enable --now zabbix-agent.service`










firewall-cmd --permanent --zone=plesk --add-rich-rule='rule family="ipv4" source address="94.143.152.30/32" port protocol="tcp" port="10050" accept'
firewall-cmd --reload


https://www.zabbix.com/documentation/3.2/en/manual/installation/install_from_packages/agent_installation

rpm -ivh http://repo.zabbix.com/zabbix/3.2/rhel/7/x86_64/zabbix-release-3.2-1.el7.noarch.rpm
yum install zabbix-agent.x86_64

modificare file 
vi /etc/zabbix/zabbix_agentd.conf
EnableRemoteCommands=1
Server=94.143.152.30
#ServerActive=127.0.0.1
Hostname=Dinamis01-94.143.153.212 [ovviamente questo cambia da server a server]

mkdir /var/lib/zabbix
chown -R zabbix:zabbix /var/lib/zabbix
chmod 500 /var/lib/zabbix/
touch /var/lib/zabbix/.my.cnf
chmod 400 /var/lib/zabbix/.my.cnf
chown -R zabbix:zabbix /var/lib/zabbix

mysql -uadmin -p`cat /etc/psa/.psa.shadow`
use mysql;
CREATE USER 'zabbix'@'%' IDENTIFIED BY 'ZBX_monit_agent_PwD18';
GRANT REPLICATION CLIENT,PROCESS,SHOW DATABASES,SHOW VIEW ON *.* TO 'zabbix'@'%';

systemctl start zabbix-agent
systemctl enable zabbix-agent.service


ct158107.vps.mvmnet.com
