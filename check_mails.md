# elenco di snippet per gestire le mail da diversi programmi

- inviare una mail di prova

`echo "Questa è una prova" | mail -s "TEST" ivano.borsato@mvmnet.com`

- controllare i messaggi in coda con exim
`exim -bp`

- controllare i messaggi incoda da uno specoifico dominio con exim
`exim -bp | grep -B1 -C0 '<DOMINIO>'`

- mostrare l'header o il dody di un specifico messaggio
`exim -Mvh <ID MAIL>`
`exim -Mvb <ID MAIL>`

- estrarre dalla lista dei messaggi in coda quelli nello stato di FROZEN
`exim -bp | grep 'frozen'`

- rimuovi le mail da un mittene specifico
`exiqgrep -i -f <MITTENTE> | xargs exim -Mrm`

- rimuovi dalla coda tutte le mail vecchie di 3 giorni
`exiqgrep -i -o 259200 | xargs exim -Mrm`

- rimuovi tutte le mail con la dicitura SPAM
`exiqgrep -i -f “^<>$” | xargs exim -Mrm`

- rimuovi le mail da un specifico dominio
`exiqgrep -i -f <DOMINIO> | xargs exim -Mrm`

- libera/cancella TUTTA la coda delle mail
`exiqgrep -i -f | xargs exim -Mrm`
`exim -bp | exiqgrep -i | xargs exim -Mrm`

- esegui la coda delle mail
`runq -qqff`

- esegui una lista degli account con un numero di mail spedite
`exim -bp | exiqsumm`

- eliminare tutte le mail dalla coda che hanno un specifico dominio e sono in stato FROZEN
`exim -bp | grep -B1 -C0 'DOMINIO'  | grep 'frozen' | grep "<" | awk {'print $3'} | xargs exim -Mrm`

## controllare mail spediti via php [sendmail, exim, postfix]

- filtrare le mail spedite via postfix

`cat /var/log/maillog | grep "postfix/smtp" | grep -P 'status=sent'`

- filtrare le mail spedite e ordinarle

`cat /var/log/maillog | grep "postfix/smtp" | grep -P 'status=sent' | sed "s/^.*: \(.\+\):.* to=<\(.\+\)>.* status=\([^ ]\+\) (\(.*\))$/[\1] <\2> \3: \4/" | sort | uniq`

## usare  pflogsumm per generare in automatico un report delle mail spedite da postfix

- installare via YUM il pkg **postfix-perl-scripts**

`yum install postfix-perl-scripts`

- eseguire il commando per avere il sommario del giorno prima delle mail inviate

`pflogsumm -d yesterday /var/log/maillog`

- eseguire questo commando per avere un riassunto delle mail inviate nella settimana scorsa

`pflogsumm /var/log/maillog.0`

- attivare schedulazione settimanale dei report di pflogsum **via cron tab**

`10 4 * * 0   /usr/local/sbin/pflogsumm /var/log/syslog.0 2>&1 |/usr/bin/mailx -s "`uname -n` weekly mail stats" postmaster`