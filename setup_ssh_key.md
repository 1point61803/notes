# How to craete the SSH keys to connect with ansible from central server to target servers

- create the SSH keys

`ssh-keygen -t rsa -b 2048 -f ~/.ssh/<NAME KEY SSH> -C "<COMMENT KEY>"`

- the keys is possible to create with password or without it

- verify and change permission for the keys

`chmod 700 ~/.ssh && chmod 600 ~/.ssh/*`

- copy the pub key to the target servers

`ssh-copy-id -i ~/.ssh/<NAME KEY SSH>  -p <PORT> <USER>@<IP TARGET>`
es.
`ssh-copy-id -i ~/.ssh/KEY_LXC_ALP_root.pub      -p 22022 root@10.20.0.40`

- test the new connections from the central server

`ssh -v -i ~/.ssh/<NAME KEY SSH> -p <PORT> <USER>@<IP TARGET>`

- enable public key ssh login in the target sshd config [/etc/ssh/sshd_config]

```
PubkeyAuthentication yes
```

- execute ansible with [StrictHostKeyChecking=no] option 

`ansible_ssh_common_args='-o StrictHostKeyChecking=no'`
