# contro0llo lo stato di ceph

- esamino l'uso di ceph dello storage
`ceph df detail`

- verifico la versione di ceph
`ceph --version`

- ottenere dettagli configurazioni dei pool

`ceph osd pool ls detail`

- verifico lo stato dei servizi ceph
`ceph -s`

- verifico l'uso dello pazio disponibile dei pools
 `rados df`

- verificare lo stato degli autoscale
`ceph osd pool autoscale-status`

- come vinee calcolato l'utilizzo dello ceph storage
%USED ~= STORED / (STORED + MAX AVAIL)