# procedure to chenge the hostanme in a node of proxmox cluster

- shutdown ALL VMs and CTs

- edit the hostanme in the file /etc/hostname and /etc/hosts

- reboot the host

- move all files of the old pve hostname folder to the new one

`cp /etc/pve/nodes/<OLD hostname>/lxc /etc/pve/nodes/<NEW hostname>/lxc`
`cp /etc/pve/nodes/<OLD hostname>/qemu-server /etc/pve/nodes/<NEW hostname>/qemu-server`

- check the new setting updated into the web UI

- if ALL is correct backup the old hostname config folder somewhere

`tar -czf /path/to/BACKUP_OLD_HOSTNAME.tar.gz /etc/pve/nodes/<OLD hostname>`
