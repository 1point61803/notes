# controllare sui log in una installazione di plesk attacchi brute force su file php

- creare lo script nella /

```
for DOMAIN in *; do
    if [ -d "$DOMAIN" ]; then
        echo "---------------------"
        echo $DOMAIN
        echo "BRUTEFORCE on xml"
        egrep -e 'POST.*xml' $DOMAIN/logs/access_ssl_log | wc -l
        echo "BRUTEFORCE on login"
        egrep -e 'POST.*login' $DOMAIN/logs/access_ssl_log | wc -l
    fi
done
```

- abilitare i permessi di esecuzione

`chmod +x /root/CHECK_BRUTE`

- spostarsi nell acartella contenente i log degli accessi standard e ssl **/var/www/vhosts/system**

`cd /var/www/vhosts/system`

- eseguire lo script e consultare il numero di tentativi per dominio

`/root/CHECK_BRUTE`
