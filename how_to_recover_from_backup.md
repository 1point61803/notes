# procedura per un recupero di una VM da backup

- si individua un backup valido nella interfaccia di PVE

- si ripristina il backup indicando un nuovo VMID e selzionando il selettore UNIQUE (cambia mac address e altre configurazioni)

- si configura una VLAN nella scheda di rete virtualizzata diversa rispetto alla VM originale

- se neccessario si cambio la password di root via tool di proxmox
`qm guest passwd <vmid> <username>`

- eseguire il boot della VM recovery

- cambiare l' hostname

- disabilitare tutti i servizi che potrebbero creare problemi con licenze(tipo panneli di controllo, plesk, altro)

- configurare l'interfaccia di rete e inizialarla