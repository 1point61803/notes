# avanzamneto ceph da 14 a 15

## passagi di controllo prima dell avanzamento di versione
- avere ceph alla versione 14.2.9 o superiore
- nell aggiornamento al primo lancio del demone che gestisce OSD può prendere diverso tempo nella fase di formattazione in base alla mole di dati da trattare
- individuare in quali nodi risulta presente il **ceph monitor**

## procedura da effettuare su ogni nodo
- cambiare i repo verso i octopus

`sed -i 's/nautilus/octopus/' /etc/apt/sources.list.d/ceph.list`
- estromettere il nodo ceph dal cluster 

`ceph osd set noout`
- effettuare l'avanzamento di versione

`apt update && apt full-upgrade`
- dopo aver effettuato l'aggiornamento su tutti i nodi riavviare il servizio di monitoraggio dove presente **ceph monitor**

`systemctl restart ceph-mon.target`
- verifcare la nuova versione di ceph

`ceph mon dump | grep min_mon_release` <<--15
- successivamente riavviare in tutti i nodi il servizio **ceph manager**

`systemctl restart ceph-mgr.target`
- verificare il coretto funzionamaneto eseguendo

`ceph -s` <<-- mgr: foo(active)
- riavviare **in un nodo alla volta** il servizio **OSD**, il riavvio richiederà del tempo per effettura la migrazione

`systemctl restart ceph-osd.target`
c'è la possibiltà di disabilitare questa migrazione eseguendo il commando sotto riportato prima di eseguire il restart del servizio OSD, ma va comunque fatta:
`ceph config set osd bluestore_fsck_quick_fix_on_mount false`

- verificare il coretto funzionamaneto eseguendo

`ceph -s` <<-- HEALTH_OK oppure HEALTH_WARN noout flag(s) set

- abiliatre il cluster ceph solo nella versione 15

`ceph osd require-osd-release octopus`
- prendere nota di ogni servizio **MDS metadata server** relativo ad ogni **ceph FS**e dei rispettivi rank impostati
- ridurre il numero di rank di ogni MDs a 1

`ceph fs set $FS_NAME max_mds 1`
aspettare che il cambio venga attuato dal cluset ceph
    
    ceph status
    ceph fs get <fs_name> | grep max_mds
    ceph fs set <fs_name> max_mds 1

- fermare i servizi mds sopsesi sui rispettivi host

`systemctl stop ceph-mds.target`
verificare la sola presenza di 1 MDS sul rank 0
- riavviare forzando l'aggiornamento del unico servizio MDS 

`systemctl restart ceph-mds.target`
- riavviare tutti i servizi MDS terminati precedentemente sui i vari nodi

`systemctl start ceph-mds.target`
- ripristinare il numero di ranks per il MDS

`ceph fs set $FS_NAME max_mds $RANKS_MAX`
- reinserire il nodo nel ceph cluster

`ceph osd unset noout`
- abilitare in ogni nodo la seconda verisione del protocollo

`ceph mon enable-msgr2`
controllare che le porte abilitate siano 6789 ->v1 3300 ->v2
- verifcare la possiblità di attivare autoscaling sui pool appena aggiornati [1](https://forum.proxmox.com/threads/ceph-octopus-upgrade-notes-think-twice-before-enabling-auto-scale.80105/)
 
`ceph osd pool set $POOLNAME pg_autoscale_mode on`