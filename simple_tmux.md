# Simple notes for tmux function and tips

- Structure of tmux software

`socket -> session -> windows -> pane`

- for close the pane in use you need to close the shell terminal emulator via exit command

- The default COMBO KEY is CTRL + B

- You can use a command line with this action (CTRL == ^):

```
<CTRL + b>
: <CMD>
```
es. :ls

- detach client from terminal emulator

^b + d

- attach to the last session

`tmux attach`

- split horizontal view

`^b + "`

- split vertical view

`^b + %`

- move from pane in a window

`^b + <ARROW>` 

- create new window

`^b + c`

- move to the next window

`^b + n`

- select specific window (by number)

`^b + 0-9`

- rename window

`^b + '`

- zoom on the pane

`^b + z`

- select a specific session

`^b + s`

- switch to the previous session

`^b + (`

- switch to the next session

`^b + )`

- execute the same xcommand on more panes

`^b + : <CMD> set syncronize-panes`

- list all session

`tmux list-sessions`

- list all window

`tmux lis-windows`

- attach a session

`tmux attach-session -t <SESSION NAME>`

- start tmux with 3 pane

`tmux new-session \; split-window -h \; split-window -v \; attach`
