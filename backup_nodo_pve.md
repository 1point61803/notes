# come implementare un backup del nodo su proxmomx back server

- creare su Proxmomx Back Server un nuovo account dedicato solo alla gestione dei backup dei vari nodi
- creare sempre su PBS un nuovo token per l'account precedentemente creato
- craere su PBS le autorizzazioni pèer il datastore che dovranno accogliere i backup [sia per l'account che per il token]
- accedere via SSH nodo su cui si vuole eseguire il backup
- mettere come variabile d'ambiente il secret del token creato precedentemente
`export PBS_PASSWORD='4b170c7b-e479-4829-b2ba-77c7db173977' && echo $PBS_PASSWORD`
- eseguire questo commando per fare il backup
 `proxmox-backup-client backup root.pxar:/ --repository 'backup_user@pbs!token_backup_pve@172.17.0.147:8007:one-shot'`

 ## commandi utili per verificare lo stato dei permessi

 - ottenere lista degli utenti
`proxmox-backup-manager user list`

- ottenere lista token di un specifico utente
`proxmox-backup-manager user list-tokens <user>@pbs`

- verificare i permessi dell utente sullo storage
`proxmox-backup-manager user permissions <user>@pbs --path /datastore/<storage>`

- verificare i permessi dell token legato ad un utenete sullo storage
`proxmox-backup-manager user permissions <user>@pbs!<token> --path /datastore/<storage>`

